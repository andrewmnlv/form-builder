export const FIELD_CHECKBOX = 'field_checkbox'
export const FIELD_FILE = 'field_file'
export const FIELD_INPUT = 'field_input'
export const FIELD_RADIO = 'field_radio'
export const FIELD_SELECT = 'field_select'
export const FIELD_TEXTAREA = 'field_textarea'

export const FIELD_TYPES_WITH_OPTIONS = [FIELD_CHECKBOX, FIELD_RADIO, FIELD_SELECT]
