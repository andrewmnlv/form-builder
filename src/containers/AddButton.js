import { connect } from 'react-redux'
import { addFormField } from '../actions'
import Button from '../components/Button'

const mapDispatchToProps = (dispatch, ownProps) => {
  return {
    onClick: () => {
      dispatch(addFormField(ownProps.type))
    }
  }
}

const AddButton = connect(null, mapDispatchToProps)(Button)

export default AddButton
