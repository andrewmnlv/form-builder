import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../actions'
import FieldList from '../components/FieldList'
import { getFieldErrorsObj, getFields } from '../reducers'

const mapStateToProps = (state) => {
  return {
    fields: getFields(state),
    fieldErrors: getFieldErrorsObj(state)
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch)
})

const FormFieldList = connect(
  mapStateToProps,
  mapDispatchToProps
)(FieldList)

export default FormFieldList
