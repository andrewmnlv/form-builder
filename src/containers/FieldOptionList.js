import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import * as actions from '../actions'
import OptionList from '../components/OptionList'
import { getOptionErrorsObj, getOptionsOfFieldWithId } from '../reducers'

const mapStateToProps = (state, props) => {
  const getFieldOptions = getOptionsOfFieldWithId()
  return {
    options: getFieldOptions(state, props),
    optionErrors: getOptionErrorsObj(state)
  }
}

const mapDispatchToProps = dispatch => ({
  actions: bindActionCreators(actions, dispatch)
})

const FieldOptionList = connect(
  mapStateToProps,
  mapDispatchToProps
)(OptionList)

export default FieldOptionList
