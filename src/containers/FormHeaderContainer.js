import { connect } from 'react-redux'
import FormHeader from '../components/FormHeader'
import { validateFields } from '../reducers/fields'
import { validateOptions } from '../reducers/options'

const FormHeaderContainer = connect(
  null,
  { validateFields, validateOptions }
)(FormHeader)

export default FormHeaderContainer
