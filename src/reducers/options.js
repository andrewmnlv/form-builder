import * as actions from '../actions'
import { getAllFields } from './fields'

const ids = (state, action) => {
  switch (action.type) {
    case actions.ADD_FORM_OPTION:
      return [...state, action.payload.id]

    case actions.DELETE_FORM_OPTION:
      return state.filter(id => id !== action.payload.id)

    default:
      return state
  }
}

const deleteOption = (state, id) => {
  state = { ...state }
  delete state[id]
  return state
}

const option = (state, action) => {
  switch (action.type) {
    case actions.ADD_FORM_OPTION:
      return {
        id: action.payload.id,
        title: ''
      }

    case actions.EDIT_FORM_OPTION_TITLE:
      return { ...state, title: action.payload.title }

    default:
      return state
  }
}

const initialState = {
  ids: [],
  entities: {}
}

export default function options (state = initialState, action) {
  switch (action.type) {
    case actions.ADD_FORM_OPTION:
    case actions.EDIT_FORM_OPTION_TITLE:
      return {
        ids: ids(state.ids, action),
        entities: {
          ...state.entities,
          [action.payload.id]: option(state.entities[action.payload.id], action)
        }
      }

    case actions.DELETE_FORM_OPTION:
      return {
        ids: ids(state.ids, action),
        entities: deleteOption(state.entities, action.payload.id)
      }

    default:
      return state
  }
}

export const getAllOptions = (state) => state.ids.map(id => state.entities[id])

export const validateOptions = () => (dispatch, getState) => {
  const state = getState()
  const options = getAllOptions(state.options)
  const fields = getAllFields(state.fields)
  const optionErrors = {}

  options.forEach(option => {
    const title = option.title
    if (!title) {
      optionErrors[option.id] = 'Title is required'
    }
  })

  fields.forEach(field => {
    const uniqueOptionTitle = new Set()
    const options = field.options.map(id => state.options.entities[id])

    options.forEach(option => {
      const title = option.title
      if (title) {
        if (uniqueOptionTitle.has(title)) {
          optionErrors[option.id] = 'Title must bu unique'
        } else {
          uniqueOptionTitle.add(title)
        }
      }
    })
  })

  dispatch(actions.setFormOptionErrors(optionErrors))
}
