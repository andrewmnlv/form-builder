import * as actions from '../actions'

const initialState = {
  fields: {},
  options: {}
}

export default function errors (state = initialState, action) {
  switch (action.type) {
    case actions.SET_FORM_FIELD_ERRORS:
      return { ...state, fields: action.payload.errors }

    case actions.SET_FORM_OPTION_ERRORS:
      return { ...state, options: action.payload.errors }

    default:
      return state
  }
}

export const getFieldErrors = state => state.fields

export const getOptionErrors = state => state.options
