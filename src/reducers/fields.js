import * as actions from '../actions'
import { FIELD_TYPES_WITH_OPTIONS } from '../constants/FieldTypes'

const ids = (state, action) => {
  switch (action.type) {
    case actions.ADD_FORM_FIELD:
      return [...state, action.payload.fieldId]

    case actions.DELETE_FORM_FIELD:
      return state.filter(id => id !== action.payload.fieldId)

    case actions.MOVE_FORM_FIELD_UP:
      const upIndex = state.indexOf(action.payload.fieldId)
      const upArr = state.filter(id => id !== action.payload.fieldId)
      upArr.splice(upIndex ? (upIndex - 1) : state.length, 0, action.payload.fieldId)
      return upArr

    case actions.MOVE_FORM_FIELD_DOWN:
      const downIndex = state.indexOf(action.payload.fieldId)
      const downArr = state.filter(id => id !== action.payload.fieldId)
      if (downIndex < state.length - 1) {
        downArr.splice(downIndex + 1, 0, action.payload.fieldId)
      } else {
        downArr.unshift(action.payload.fieldId)
      }
      return downArr

    default:
      return state
  }
}

const deleteField = (state, id) => {
  state = { ...state }
  delete state[id]
  return state
}

const optionIds = (state, action) => {
  switch (action.type) {
    case actions.ADD_FORM_FIELD_OPTION:
      return [...state, action.payload.optionId]

    case actions.DELETE_FORM_FIELD_OPTION:
      return state.filter(id => id !== action.payload.optionId)

    default:
      return state
  }
}

const field = (state, action) => {
  switch (action.type) {
    case actions.ADD_FORM_FIELD:
      return {
        id: action.payload.fieldId,
        type: action.payload.type,
        title: '',
        required: false,
        options: []
      }

    case actions.TOGGLE_FORM_FIELD_REQUIRED:
      return { ...state, required: !state.required }

    case actions.EDIT_FORM_FIELD_TITLE:
      return { ...state, title: action.payload.title }

    case actions.ADD_FORM_FIELD_OPTION:
    case actions.DELETE_FORM_FIELD_OPTION:
      return { ...state, options: optionIds(state.options, action) }

    default:
      return state
  }
}

const initialState = {
  ids: [],
  entities: {}
}

export default function fields (state = initialState, action) {
  switch (action.type) {
    case actions.ADD_FORM_FIELD:
    case actions.TOGGLE_FORM_FIELD_REQUIRED:
    case actions.EDIT_FORM_FIELD_TITLE:
    case actions.ADD_FORM_FIELD_OPTION:
    case actions.DELETE_FORM_FIELD_OPTION:
    case actions.MOVE_FORM_FIELD_UP:
    case actions.MOVE_FORM_FIELD_DOWN:
      return {
        ids: ids(state.ids, action),
        entities: {
          ...state.entities,
          [action.payload.fieldId]: field(state.entities[action.payload.fieldId], action)
        }
      }

    case actions.DELETE_FORM_FIELD:
      return {
        ids: ids(state.ids, action),
        entities: deleteField(state.entities, action.payload.fieldId)
      }

    default:
      return state
  }
}

export const getAllFields = (state) => state.ids.map(id => state.entities[id])

export const getFieldWithId = (state, id) => state.entities[id]

export const validateFields = () => (dispatch, getState) => {
  const state = getState()
  const fields = getAllFields(state.fields)
  const fieldErrors = {}
  const uniqueFieldTitle = new Set()

  fields.forEach(field => {
    const title = field.title

    if (~FIELD_TYPES_WITH_OPTIONS.indexOf(field.type) && field.options.length === 0) {
      fieldErrors[field.id] = 'Field must contain choices'
    }

    if (title) {
      if (uniqueFieldTitle.has(title)) {
        fieldErrors[field.id] = 'Title must bu unique'
      } else {
        uniqueFieldTitle.add(title)
      }
    } else {
      fieldErrors[field.id] = 'Title is required'
    }
  })

  dispatch(actions.setFormFieldErrors(fieldErrors))
}
