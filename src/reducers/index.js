import { combineReducers } from 'redux'
import { createSelector } from 'reselect'
import errors, { getFieldErrors, getOptionErrors } from './errors'
import fields, { getAllFields, getFieldWithId } from './fields'
import options, { getAllOptions } from './options'

const rootReducer = combineReducers({
  errors,
  fields,
  options
})

export default rootReducer

const getOptions = state => getAllOptions(state.options)

export const getFields = state => getAllFields(state.fields)

const getField = (state, props) => {
  return getFieldWithId(state.fields, props.field.id)
}

export const getOptionsOfFieldWithId = () => {
  return createSelector(getOptions, getField, (options, field) => field.options.map(id => options[id]))
}

export const getFieldErrorsObj = state => getFieldErrors(state.errors)
export const getOptionErrorsObj = state => getOptionErrors(state.errors)
