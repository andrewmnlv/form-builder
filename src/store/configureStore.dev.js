import { applyMiddleware, compose, createStore } from 'redux'
import reduxImmutableStateInvariant from 'redux-immutable-state-invariant'
import thunk from 'redux-thunk'
import rootReducer from '../reducers'

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose

const configureStore = initialState => createStore(
  rootReducer,
  initialState,
  composeEnhancers(
    applyMiddleware(thunk, reduxImmutableStateInvariant())
  )
)

export default configureStore
