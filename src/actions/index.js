// Fields

export const ADD_FORM_FIELD = 'ADD_FORM_FIELD'
export const DELETE_FORM_FIELD = 'DELETE_FORM_FIELD'
export const TOGGLE_FORM_FIELD_REQUIRED = 'TOGGLE_FORM_FIELD_REQUIRED'
export const EDIT_FORM_FIELD_TITLE = 'EDIT_FORM_FIELD_TITLE'
export const MOVE_FORM_FIELD_UP = 'MOVE_FORM_FIELD_UP'
export const MOVE_FORM_FIELD_DOWN = 'MOVE_FORM_FIELD_DOWN'
export const ADD_FORM_FIELD_OPTION = 'ADD_FORM_FIELD_OPTION'
export const DELETE_FORM_FIELD_OPTION = 'DELETE_FORM_FIELD_OPTION'

let nextFormFieldId = 0
export const addFormField = type => ({ type: ADD_FORM_FIELD, payload: { fieldId: nextFormFieldId++, type } })
export const deleteFormField = fieldId => ({ type: DELETE_FORM_FIELD, payload: { fieldId } })
export const toggleFormFieldRequired = fieldId => ({ type: TOGGLE_FORM_FIELD_REQUIRED, payload: { fieldId } })
export const editFormFieldTitle = (fieldId, title) => ({ type: EDIT_FORM_FIELD_TITLE, payload: { fieldId, title } })
export const moveFormFieldUp = (fieldId) => ({ type: MOVE_FORM_FIELD_UP, payload: { fieldId } })
export const moveFormFieldDown = (fieldId) => ({ type: MOVE_FORM_FIELD_DOWN, payload: { fieldId } })
export const addFormFieldOption = (fieldId, optionId) => ({
  type: ADD_FORM_FIELD_OPTION,
  payload: { fieldId, optionId }
})
export const deleteFormFieldOption = (fieldId, optionId) => ({
  type: DELETE_FORM_FIELD_OPTION,
  payload: { fieldId, optionId }
})

// Options

export const ADD_FORM_OPTION = 'ADD_FORM_OPTION'
export const DELETE_FORM_OPTION = 'DELETE_FORM_OPTION'
export const EDIT_FORM_OPTION_TITLE = 'EDIT_FORM_OPTION_TITLE'

let nextFormOptionId = 0
export const addFormOption = () => ({ type: ADD_FORM_OPTION, payload: { id: nextFormOptionId++ } })
export const deleteFormOption = id => ({ type: DELETE_FORM_OPTION, payload: { id } })
export const editFormTitleOption = (id, title) => ({ type: EDIT_FORM_OPTION_TITLE, payload: { id, title } })

// Errors

export const SET_FORM_FIELD_ERRORS = 'SET_FORM_FIELD_ERRORS'
export const SET_FORM_OPTION_ERRORS = 'SET_FORM_OPTION_ERRORS'

export const setFormFieldErrors = (errors) => ({ type: SET_FORM_FIELD_ERRORS, payload: { errors } })
export const setFormOptionErrors = (errors) => ({ type: SET_FORM_OPTION_ERRORS, payload: { errors } })
