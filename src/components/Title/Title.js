import React, { Component, PropTypes } from 'react'
import TitleInput from '../TitleInput'
import './Title.css'

export default class Title extends Component {
  static propTypes = {
    title: PropTypes.string,
    error: PropTypes.string,
    required: PropTypes.bool,
    onSave: PropTypes.func.isRequired,
    onDelete: PropTypes.func
  }

  state = {
    editing: false
  }

  handleEdit = () => {
    this.setState({ editing: true })
  }

  handleSave = (title) => {
    this.props.onSave(title)
    this.setState({ editing: false })
  }

  handleDelete = () => {
    this.props.onDelete()
  }

  render () {
    const { title, error, required, onDelete } = this.props
    const text = title ? <span className='fb_title__text'>{title}</span> : ''
    const asterisk = required ? <span className='fb_title__asterisk'>*</span> : ''
    let res, remove

    if (onDelete) {
      remove = <button
        type='button'
        className='btn btn-outline-secondary'
        onClick={() => this.handleDelete()}>Remove</button>
    }

    if (this.state.editing) {
      res =
        <TitleInput
          title={title}
          onSave={(title) => this.handleSave(title)} />
    } else {
      res = <div>
        {text}
        {asterisk}
        <button
          type='button'
          className='btn btn-outline-primary'
          onClick={this.handleEdit}>Edit title
        </button>
        {remove}
      </div>
    }
    return (
      <div className='fb_title'>
        {res}
        <div className='fb_title__error'>{error}</div>
      </div>
    )
  }
}
