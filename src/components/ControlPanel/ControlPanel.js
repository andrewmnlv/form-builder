import React from 'react'
import {
  FIELD_CHECKBOX,
  FIELD_FILE,
  FIELD_INPUT,
  FIELD_RADIO,
  FIELD_SELECT,
  FIELD_TEXTAREA
} from '../../constants/FieldTypes'
import AddButton from '../../containers/AddButton'
import './ControlPanel.css'

const buttons = [
  { type: FIELD_INPUT, title: 'Single-line text' },
  { type: FIELD_RADIO, title: 'Radio button' },
  { type: FIELD_CHECKBOX, title: 'Checkboxes' },
  { type: FIELD_SELECT, title: 'Select' },
  { type: FIELD_FILE, title: 'File upload' },
  { type: FIELD_TEXTAREA, title: 'Paragraph text' }
]

const ControlPanel = () => (
  <div className='fb_control-panel row'>
    {buttons.map((button, idx) =>
      <div className='col-6' key={idx}>
        <AddButton type={button.type}>{button.title}</AddButton>
      </div>
    )}
  </div>
)

export default ControlPanel
