import React, { PropTypes } from 'react'

const Button = ({ children, onClick }) => {
  return (
    <button
      type='button'
      className='btn btn-outline-primary btn-block'
      onClick={e => {
        e.preventDefault()
        onClick()
      }}>
      {children}
    </button>
  )
}

Button.propTypes = {
  children: PropTypes.node.isRequired,
  onClick: PropTypes.func.isRequired
}

export default Button
