import React, { PropTypes } from 'react'
import './FormHeader.css'

const FormHeader = ({ validateFields, validateOptions }) => (
  <div className='fb_form-header'>
    <div className='row'>
      <div className='fb_form-header__title col-10'>San Francisco Driver Form</div>
      <div className='fb_form-header__save col-2'>
        <button
          type='button'
          className='btn btn-primary btn-sm'
          onClick={() => {
            validateFields()
            validateOptions()
          }}>Save form
        </button>
      </div>
    </div>
    <div className='fb_form-header__description'>
      <span>DESCRIPTION: </span>
      Welcome aboard!
    </div>
  </div>
)

FormHeader.propTypes = {
  validateFields: PropTypes.func.isRequired,
  validateOptions: PropTypes.func.isRequired
}

export default FormHeader
