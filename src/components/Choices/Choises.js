import React, { Component, PropTypes } from 'react'
import {
  FIELD_FILE,
  FIELD_INPUT,
  FIELD_SELECT,
  FIELD_TEXTAREA,
  FIELD_TYPES_WITH_OPTIONS
} from '../../constants/FieldTypes'
import FieldOptionList from '../../containers/FieldOptionList'

export default class Choices extends Component {
  static propTypes = {
    field: PropTypes.object.isRequired,
    addFormOption: PropTypes.func.isRequired,
    addFormFieldOption: PropTypes.func.isRequired
  }

  handleAddOption = () => {
    const { field, addFormOption, addFormFieldOption } = this.props
    const optionId = addFormOption().payload.id
    addFormFieldOption(field.id, optionId)
  }

  render () {
    const { field } = this.props
    let options, preview

    switch (field.type) {
      case FIELD_INPUT:
        preview = <input type='text' className='form-control' placeholder={field.title} />
        break

      case FIELD_TEXTAREA:
        preview = <textarea className='form-control' placeholder={field.title} />
        break

      case FIELD_FILE:
        preview = <input type='file' className='form-control' />
        break

      case FIELD_SELECT:
        preview = <select className='form-control'>
          <option>Choose option</option>
        </select>
        break

      default:
        break
    }

    if (~FIELD_TYPES_WITH_OPTIONS.indexOf(field.type)) {
      options = <div>
        <FieldOptionList field={field} />
        <button
          type='button'
          className='btn btn-outline-primary'
          onClick={this.handleAddOption}>+ Add Choice
        </button>
      </div>
    }

    preview = preview ? <div className='form-group'>{preview}</div> : null

    return (
      <div>
        {preview}
        <div>{options}</div>
      </div>
    )
  }
}
