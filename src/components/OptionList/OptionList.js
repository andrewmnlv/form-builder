import React, { PropTypes } from 'react'
import OptionItem from '../OptionItem'
import './OptionList.css'

const OptionList = ({ options, optionErrors, actions, field }) => (
  <ul className='fb_option-list'>
    {options.map(option =>
      <li key={option.id} className='fb_option-list__item'>
        <OptionItem option={option} actions={actions} field={field} error={optionErrors[option.id]} />
      </li>
    )}
  </ul>
)

OptionList.propTypes = {
  options: PropTypes.array.isRequired,
  optionErrors: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired,
  field: PropTypes.object.isRequired
}

export default OptionList
