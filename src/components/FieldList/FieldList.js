import React, { PropTypes } from 'react'
import FieldItem from '../FieldItem'
import './FieldList.css'

const FieldList = ({ fields, fieldErrors, actions }) => (
  <div className='fb_field-list'>
    <div className='row'>
      <div className='col-1' />
      <div className='col-3'>QUESTION TITLE</div>
      <div className='col-6'>CHOICES</div>
      <div className='col-1'>REQUIRED?</div>
      <div className='col-1' />
    </div>
    <ul className='fb_field-list__fields'>
      {fields.map(field =>
        <li className='fb_field-list__item' key={field.id}>
          <FieldItem field={field} error={fieldErrors[field.id]} actions={actions} />
        </li>
      )}
    </ul>
  </div>
)

FieldList.propTypes = {
  fields: PropTypes.array.isRequired,
  fieldErrors: PropTypes.object.isRequired,
  actions: PropTypes.object.isRequired
}

export default FieldList
