import React, { Component, PropTypes } from 'react'

export default class TitleInput extends Component {
  static propTypes = {
    onSave: PropTypes.func.isRequired,
    title: PropTypes.string,
    placeholder: PropTypes.string
  }

  state = {
    title: this.props.title || ''
  }

  handleSubmit = e => {
    const title = e.target.value.trim()
    if (e.which === 13) {
      this.props.onSave(title)
    }
  }

  handleChange = e => {
    this.setState({ title: e.target.value })
  }

  handleBlur = e => {
    this.props.onSave(e.target.value)
  }

  render () {
    return (
      <input
        type='text'
        className='form-control'
        placeholder={this.props.placeholder}
        autoFocus='true'
        value={this.state.title}
        onBlur={this.handleBlur}
        onChange={this.handleChange}
        onKeyDown={this.handleSubmit} />
    )
  }
}
