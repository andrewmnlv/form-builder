import React from 'react'
import FormFieldList from '../../containers/FormFieldList'
import FormHeaderContainer from '../../containers/FormHeaderContainer'
import Sidebar from '../Sidebar'
import './App.css'

export default () => (
  <div className='fb_app'>
    <aside className='fb_app__aside'>
      <Sidebar />
    </aside>
    <main className='fb_app__main'>
      <div className='fb_app__editor'>
        <FormHeaderContainer />
        <hr />
        <FormFieldList />
      </div>
    </main>
  </div>
)
