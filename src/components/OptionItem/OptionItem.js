import React, { Component, PropTypes } from 'react'
import { FIELD_CHECKBOX, FIELD_RADIO, FIELD_SELECT } from '../../constants/FieldTypes'
import Title from '../Title'
import './OptionItem.css'

export default class OptionItem extends Component {
  static propTypes = {
    option: PropTypes.object.isRequired,
    error: PropTypes.string,
    field: PropTypes.object.isRequired,
    actions: PropTypes.shape({
      editFormTitleOption: PropTypes.func.isRequired,
      deleteFormFieldOption: PropTypes.func.isRequired
    }).isRequired
  }

  handleSave = (id, title) => {
    this.props.actions.editFormTitleOption(id, title)
  }

  handleDelete = (fieldId, optionId) => {
    this.props.actions.deleteFormFieldOption(fieldId, optionId)
  }

  render () {
    const { option, field, error } = this.props
    let title, preview

    title = <Title
      title={option.title}
      error={error}
      onDelete={() => this.handleDelete(field.id, option.id)}
      onSave={(title) => this.handleSave(option.id, title)} />

    switch (field.type) {
      case FIELD_CHECKBOX:
        preview = <div className='form-check'>
          <label className='form-check-label'>
            <input className='form-check-input' type='checkbox' defaultChecked />
          </label>
        </div>
        break
      case FIELD_RADIO:
        preview = <div className='form-check'>
          <label className='form-check-label'>
            <input className='form-check-input' type='radio' defaultChecked />
          </label>
        </div>
        break
      case FIELD_SELECT:
      default:
        break
    }

    return (
      <div className='fb_option-item'>
        <div className='fb_option-item__preview'>{preview}</div>
        {title}
      </div>
    )
  }
}
