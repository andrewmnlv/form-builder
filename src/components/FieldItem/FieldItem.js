import React, { Component, PropTypes } from 'react'
import Choices from '../Choices'
import Title from '../Title'

export default class FieldItem extends Component {
  static propTypes = {
    field: PropTypes.object.isRequired,
    error: PropTypes.string,
    actions: PropTypes.shape({
      editFormFieldTitle: PropTypes.func.isRequired,
      deleteFormField: PropTypes.func.isRequired,
      toggleFormFieldRequired: PropTypes.func.isRequired,
      moveFormFieldUp: PropTypes.func.isRequired,
      moveFormFieldDown: PropTypes.func.isRequired
    }).isRequired
  }

  handleSave = (id, title) => {
    this.props.actions.editFormFieldTitle(id, title)
  }

  handleMoveFieldUp = () => {
    const { field, actions } = this.props
    actions.moveFormFieldUp(field.id)
  }

  handleMoveFieldDown = () => {
    const { field, actions } = this.props
    actions.moveFormFieldDown(field.id)
  }

  render () {
    const { field, actions, error } = this.props
    let sort, title, choices, required, remove

    sort = <div>
      <button
        className='btn btn-outline-secondary btn-sm'
        type='button'
        onClick={this.handleMoveFieldUp}>
        &uarr;
      </button>
      <button
        className='btn btn-outline-secondary btn-sm'
        type='button'
        onClick={this.handleMoveFieldDown}>
        &darr;
      </button>
    </div>

    title = <Title
      title={field.title}
      error={error}
      required={field.required}
      onSave={(title) => this.handleSave(field.id, title)} />

    choices = <Choices field={field} {...actions} />

    required = <div className='form-check'>
      <label className='form-check-label'>
        <input
          type='checkbox'
          className='form-check-input'
          checked={field.required}
          onChange={() => actions.toggleFormFieldRequired(field.id)} />
      </label>
    </div>

    remove = <button
      type='button'
      className='btn btn-outline-danger'
      onClick={() => actions.deleteFormField(field.id)}>Remove</button>

    return (
      <div className='row'>
        <div className='col-1'>{sort}</div>
        <div className='col-3'>{title}</div>
        <div className='col-6'>{choices}</div>
        <div className='col-1'>{required}</div>
        <div className='col-1'>{remove}</div>
      </div>
    )
  }
}
