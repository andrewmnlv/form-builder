import React from 'react'
import { Tab, TabList, TabPanel, Tabs } from 'react-tabs'
import ControlPanel from '../ControlPanel'
import './Sidebar.css'

Tabs.setUseDefaultStyles(false)

export default () => (
  <div className='fb_sidebar'>
    <div className='fb_sidebar__title'>
      San Francisco Driver Form
    </div>

    <Tabs>
      <TabList>
        <Tab>Custom fields</Tab>
        <Tab>Description (Optional)</Tab>
      </TabList>

      <TabPanel>
        <div className='fb_sidebar__note'>Select fields will be added to form.</div>
        <hr />
        <div className='fb_sidebar__describe'>Add Custom Field</div>
        <ControlPanel />
      </TabPanel>
      <TabPanel>
        <div className='fb_sidebar__note'>Optional form description</div>
        <hr />
        <div className='fb_sidebar__describe'>Form Description</div>
        <textarea className='form-control' placeholder='Welcome aboard!' rows='10' />
      </TabPanel>
    </Tabs>
  </div>
)
